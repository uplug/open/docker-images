# uPlug Public Docker Images Registry: VueJS

[VueJS](https://vuejs.org/) is an open-source JavaScript framework for
building user interfaces and single-page applications.

## Tags

- `latest`: Version used for development.

  - **node**: `12.5.0`
  - **@vue/cli**: `4.0.5`
  - **@vue/cli-service-global**: `4.0.5
- `reduced`: Version used for production.

  - **node**: `12.5.0`
