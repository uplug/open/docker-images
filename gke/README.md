# uPlug Public Docker Images Registry: Google Kubernetes Engine (GKE)

This image contains the following tools:

- `gcloud`: CLI tool to manage google cloud elements
- `docker`: CLI tool to manage containers and container images
- `kubectl`: CLI tool to manage kubernetes clusters
- `kubeval`: CLI tool to validate kubernetes templates

## Tags

- `latest`

  - **Google Cloud SDK**: `232.0.0`
  - **Docker**: `19.03.2`
  - **Kubernetes**: `1.16.1`
  - **Kubeval**: `0.14.0`
