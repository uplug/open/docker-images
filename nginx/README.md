# uPlug Public Docker Images Registry: NGINX

[NGINX](https://www.nginx.com/) is a web server which can also be used as a
reverse proxy, load balancer, mail proxy and HTTP cache.

## Tags

- `latest`: Version used for production.

  - **nginx**: `1.17`
