# uPlug Public Docker Images Registry: Docker Compose

[Compose](https://docs.docker.com/compose/) is a tool for defining and running
multi-container Docker applications.

## Tags

- `latest`: Version used as main source for
  [GitLab CI](https://docs.gitlab.com/ee/ci/) pipelines.

  - **docker**: `19.03.4`
  - **docker-compose**: `1.24.1`
  - **curl**: From docker image apk repository
  - **gcc**: From docker image apk repository
  - **git**: From docker image apk repository
  - **libc-dev**: From docker image apk repository
  - **libffi-dev**: From docker image apk repository
  - **make**: From docker image apk repository
  - **openssl-dev**: From docker image apk repository
  - **py-pip**: Latest version released on build
  - **python-dev**: From docker image apk repository
