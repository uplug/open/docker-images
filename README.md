# uPlug Public Docker Image Registry

## Summary

- [ansible](ansible)
- [docker-compose](docker-compose)
- [elixir](elixir)
- [erlang](erlang)
- [gke](gke)
- [google-cloud-manager](google-cloud-manager)
- [nginx](nginx)
- [phoenix](phoenix)
- [terraform](terraform)
- [vue](vue)
