# uPlug Public Docker Images Registry: Phoenix

[Phoenix](https://phoenixframework.org/) is a web development framework written in the functional programming language Elixir.

## Tags

- `latest`: Version used for development.

  - **phoenix**: `1.4.13`
  - **elixir**: From uPlug Elixir image
  - **node**: `13.8.0`
  - **inotify**-tools: From uPlug Elixir image apt repository
- `nodeless`: Version used for development, without node.

  - **phoenix**: `1.4.13`
  - **elixir**: From uPlug Elixir image
- `alpine`: Version used for production.

  - **phoenix**: `1.4.13`
  - **elixir**: From uPlug Elixir image
  - **nodejs**: From uPlug Elixir alpine image apk repository
  - **git**: From uPlug Elixir alpine image apk repository
  - **build-base**: From uPlug Elixir alpine image apk repository
  - **yarn**: From uPlug Elixir alpine image apk repository
  - **python**: From uPlug Elixir alpine image apk repository
- `alpine-nodeless`: Version used for production, without node.

  - **phoenix**: `1.4.13`
  - **elixir**: From uPlug Elixir image
  - **git**: From uPlug Elixir alpine image apk repository
  - **build-base**: From uPlug Elixir alpine image apk repository
  - **python**: From Uplug Elixir alpine image apk repository
