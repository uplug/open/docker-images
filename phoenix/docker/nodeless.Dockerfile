FROM registry.gitlab.com/uplug/open/docker-images/elixir

RUN mix archive.install hex --force phx_new 1.4.13
