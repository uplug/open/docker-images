# uPlug Public Docker Images Registry: Terraform

This image contains the following tools:

- `terraform`: CLI tool to manage infrastructure

## Tags

- `latest`

  - **Terraform**: `0.12.7`
